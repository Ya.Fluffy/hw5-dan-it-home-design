// document.body.style.backgroundColor = '#726e8d'
// document.body.classList.add('dark-theme')
window.addEventListener('load', ()=>{
 const theme = localStorage.getItem('theme')
 console.log(theme)
 if(theme && theme === 'dark-theme'){
    document.body.classList.add('dark-theme')
 }
})
document.querySelector('.toggle-theme').addEventListener('click', ()=>{
    document.body.classList.toggle('dark-theme')
})

window.addEventListener('beforeunload', ()=>{
    if(document.body.classList.contains('dark-theme')){
       localStorage.setItem('theme', 'dark-theme')
    } else {
        localStorage.setItem('theme', 'light-theme')
    }
 
})


// Теоритичні питання:
// 1. В чому полягає відмінність localStorage і sessionStorage?
// localStorage  - зберігає дані, навіть закритті браузеру.
// sessionStorage - зберігає дані тільки на протязі однієї сесії, тобто поки відкрита вкладка.


// 2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
// паролі небезпечно зберігати в localStorage чи sessionStorage, краще зберігати на сервері. 

// 3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
// sessionStorage очищається, дані видаляються.